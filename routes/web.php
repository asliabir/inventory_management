<?php

Route::get('/', 'HomeController@index')->middleware('auth');

// auth
Auth::routes();

Route::middleware('auth')->group(function () {
    //deshboard
    Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
    // category routes
    Route::get('/category', 'CategoryController@index')->name('category');
    Route::get('/category/add', 'CategoryController@create')->name('add_category');
    Route::post('/category/add', 'CategoryController@store')->name('add_category');
    Route::get('/category/edit/{id}', 'CategoryController@edit')->name('edit_category');
    Route::put('/category/edit/{id}', 'CategoryController@update')->name('update_category');
    Route::delete('/category/delete', 'CategoryController@destroy')->name('delete_category');

    // Vendors Routes
    Route::get('/vendors', 'VendorController@index')->name('vendors');
    Route::get('/vendors/add', 'VendorController@create')->name('add_vendors');

});

