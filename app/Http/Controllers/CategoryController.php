<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index(){
        $collections = Category::orderBy('id', 'DESC')->paginate(10);
        return view('category.index')->with('collections', $collections);
    }

    public function create()
    {
        return view('category.add');
    }

    public function store(Request $request)
    {
        $category = $request->validate([
            'c_name' => 'required',
            'c_detils' => 'required',
        ]);

        $success = Category::create($category);

        if($success){
            $notification = [
                'message' => 'Category Added Successfully!',
                'alert-type' => 'success'
            ];
        } else {
            $notification = [
                'message' => 'Category Adding Failed!',
                'alert-type' => 'error'
            ];
        }
        return redirect(route('category'))->with($notification);
    }


    public function edit(Category $category, $id)
    {
        $data = $category->where('id' ,$id)->firstOrFail();
        return view('category.edit')->with('data',$data);

    }


    public function update(Request $request, Category $category, $id)
    {
        $success = $category->where('id', $id)->update([
            'c_name' => $request->c_name,
            'c_detils' => $request->c_detils
        ]);

        if($success){
            $notification = [
                'message' => 'Category Edited Successfully!',
                'alert-type' => 'success'
            ];
        } else {
            $notification = [
                'message' => 'Category Editing Failed!',
                'alert-type' => 'error'
            ];
        }
        return redirect(route('category'))->with($notification);
    }

    public function destroy(Request $request, Category $category)
    {
        $success = $category->where('id',$request->id)->delete();

        if($success){
            $notification = [
                'message' => 'Category Deleted Successfully!',
                'alert-type' => 'success'
            ];
        } else {
            $notification = [
                'message' => 'Category Deleting Failed!',
                'alert-type' => 'error'
            ];
        }
        return redirect(route('category'))->with($notification);
    }
}
