@extends('layouts.app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Edit Category</h6>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <div class="p-5">
                        <form class="category" action="{{ route('edit_category', $data['id']) }}" method="post">
                            @method('PUT')
                            @csrf
                            <div class="form-group">
                                <label for="c_name">Category Name</label>
                            <input type="text" name="c_name" class="form-control form-control-user" id="c_name" value="{{ $data['c_name'] }}">
                            </div>
                            <div class="form-group">
                                <label for="c_detils">Category Description</label>
                                <input type="text" name="c_detils" class="form-control form-control-user"
                                       id="c_detils" value="{{ $data['c_detils'] }}">
                            </div>
                            <button type="submit" name="submit" class="btn btn-primary btn-user btn-block">Submit
                            </button>
                    </div>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </div>
@endsection
