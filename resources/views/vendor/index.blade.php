@extends('layouts.app')

@section('content')
    <div class="card shadow mb-4">
        <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary d-inline">Vendor</h6> <a class="btn btn-success float-right" href="{{ route('add_vendors') }}">New Category</a>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Name</th>
                      <th>Email</th>
                      <th>Phone</th>
                      <th>Description</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Phone</th>
                        <th>Description</th>
                        <th>Action</th>
                    </tr>
                  </tfoot>
                  <tbody>
                      @foreach ($collections as $data)
                      <tr>
                        <td>{{ $data['id'] }}</td>
                        <td>{{ $data['name'] }}</td>
                        <td>{{ $data['email'] }}</td>
                        <td>{{ $data['phone'] }}</td>
                        <td>{{ $data['description'] }}</td>
                      <td><form action="{{ route('delete_category') }}" method="post"><a class="btn btn-sm btn-warning" href="{{ route('edit_category', $data['id']) }}">Edit</a> @method('DELETE') @csrf <input type="hidden" name="id" value="{{ $data['id'] }}"><button onclick="return confirm('Do you really want to delete?');" class="btn btn-sm btn-danger" type="submit">Delete</button></form></td>
                      </tr>
                      @endforeach
                  </tbody>
                </table>
                {{ $collections->links() }}
              </div>
        </div>
    </div>
@endsection
